export default {
    data() {
        return {
            isMouseDown: false,
            lasLoc: {
                x: 0,
                y: 0
            },
            lasTimeStamp: 0,
            lasLineWidth: -1,
            strokeColor: "#333333",
            el: null,
            ctx: null,
            canvasWidth: 300,
            canvasHeight: 500,
            resultLineWidth: 10,
            maxLineWidth: 10,
            minLineWidth: 1,
            maxLineSpeed: 10,
            minLineSpeed: 0.1,
            touch: null,
            currentIndex: 0
        };
    },
    onShow() {
        this.initCavans();
    },
    initCavans() {
        this.el = this.$refs.writeCanvas;
        this.el.width = this.canvasWidth;
        this.el.height = this.canvasHeight;
        this.ctx = this.el.getContext('2d', { antialias: true });
    },
    // 手指触碰事件
    touchStartFunc(e) {
        this.touch = e.touches[0];
        this.beginStock({x:this.touch.localX, y:this.touch.localY});
    },
    touchMoveFunc(e) {
        this.touch = e.touches[0];
        this.moveStock({x:this.touch.localX, y:this.touch.localY});
    },
    touchEndFunc(e) {
        this.endStock();
    },
    // 选择画笔颜色
    selectColor(index) {
        this.currentIndex = index;
        var colorArr = ['#333333', '#6d94ff', '#00ff71', '#ff6e74', '#ffec4e'];
        this.strokeColor = colorArr[index];
    },
    // 清除画布内容
    clearDraw() {
        this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
    },
    windowToCanvas(x,y) {
        return {x, y};
    },
    beginStock(point) {
        this.isMouseDown = true;
        this.lasloc = this.windowToCanvas(point.x, point.y);
        this.lasTimeStamp = new Date().getTime();
    },
    endStock(point) {
        this.isMouseDown = false;
    },
    moveStock(point) {
        var curloc = this.windowToCanvas(point.x, point.y);
        var curTimeStamp = new Date().getTime();
        var s = this.calDistangce(curloc, this.lasloc);
        var t = curTimeStamp - this.lasTimeStamp;
        var lineWidth = this.calClientWidth(t, s);
        this.ctx.beginPath();
        this.ctx.moveTo(this.lasloc.x, this.lasloc.y);
        this.ctx.lineTo(curloc.x, curloc.y);
        this.ctx.strokeStyle = this.strokeColor;
        this.ctx.lineWidth = lineWidth;
        this.ctx.lineCap = "round";
        this.ctx.lineJoin = "round";
        this.ctx.stroke();
        this.lasloc = curloc;
        this.lasTimeStamp = curTimeStamp;
        this.laslinewidth = this.resultLineWidth;
    },
    // 计算两点之间的距离
    calDistangce(loc1, loc2) {
        return Math.sqrt((loc1.x-loc2.x)*(loc1.x-loc2.x)+(loc1.y-loc2.y)*(loc1.y-loc2.y));
    },
    // 设置画笔
    calClientWidth(t,s) {
        var v = s/t;
        if(v <= this.minLineSpeed) {
            this.resultLineWidth = this.maxLineWidth;
        } else if (v >= this.maxLineSpeed) {
            this.resultLineWidth = this.minLineWidth;
        } else {
            this.resultLineWidth = this.maxLineWidth-(v-this.minLineSpeed)/(this.maxLineSpeed-this.minLineSpeed)*(this.maxLineWidth-this.minLineWidth);
        }
        if(this.laslinewidth == -1) {
            return this.resultLineWidth;
        }
        return this.laslinewidth*2/3 + this.resultLineWidth*1/3;
    },
    // 保存画板
//    saveDraw() {
//        var image = 'image/png'
//        const canvas = this.$element('myCanvas')
//        var a = canvas.toDataURL(image)
//        console.warn('asdfasdfasdf'+ a)
//    }
}
