# 自定义画板组件

### 仓库地址

- https://gitee.com/newamas/draw-board.git

### SDK版本

- compileSdkVersion 6
- compatibleSdkVersion 6

### 作者ID

- 郭迪

### 组件功能

- 自定义画板，可以自由绘制图形或签字。

### 效果图
<img src="/screenShoot/image.gif" width="260" height="462"/>